#!/bin/sh

cwd=$(pwd)

aux=$HOME/aux

dev=$HOME/dev

# dotfiles cleanup: delete broken links
find -L  ~/ -maxdepth 1 -type l -delete
find -L $dev -maxdepth 1 -type l -delete

# link dotfiles
for df in $HOME/.*; do
   [ -e "$df" ] || continue

   bdf=$(basename $df)
   [ "$bdf" = '.' ] || continue
   [ "$bdf" = '..' ] || continue

   rm -f  $dev/$bdf
   ln -s $df $dev/$bdf
done


[ -d "$aux" ] || { echo "Err: no aux in $aux" ; exit 1 ; }

[ -d "$dev" ] || mkdir -p $dev



for i in $aux/* ; do 
   [ -f "$i" ] || continue

   base=$(basename $i)
   rm -f $dev/$base
   ln -s $i $dev/
done

# link this dir to dev
bcwd=$(basename $cwd)
rm -f $dev/$bcwd
ln -s $cwd $dev/$bcwd
